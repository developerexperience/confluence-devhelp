package com.atlassian.devhelp.confluence.rest;

import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

@Path("/health")
public class HealthCheckResource {

    private static final Logger LOG = LoggerFactory.getLogger(HealthCheckResource.class);

    private final ContentService contentService;

    @Autowired
    public HealthCheckResource(
            @ComponentImport final ContentService contentService) {
        this.contentService = contentService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/ping")
    public Response ping() {
        LOG.info("** health / ping");
        return Response.ok(
                singletonMap(
                        "components",
                        singletonList(
                                contentService.toString()
                        )
                )
        ).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/content/{id}")
    public Response content(@PathParam("id") long contentId) {
        LOG.info("** health / content");
        return contentService.find()
                .withId(ContentId.of(ContentType.PAGE, contentId))
                .fetch()
                .map(item -> Response.ok(item).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }
}
