## Confluence DEVHELP playground

### requirements

* atlassian-sdk

for macOS

```bash
brew tap atlassian/tap
brew install atlassian/tap/atlassian-plugin-sdk
```

for other platforms, please take a look at https://developer.atlassian.com/server/framework/atlassian-sdk/downloads/

* mvnvm, please see https://mvnvm.org/

to make sure the atlassian sdk is using mvnvm (mvn version manager), you should update your
.bashrc with the following after installing mvnvm

```bash
export ATLAS_MVN=$(which mvn)
```

### dev setup

* terminal 1
```bash
cd plugin
atlas-run
```

* terminal 2 (to refresh the instance with new plugin updates using QuickReload)
```bash
cd plugin
atlas-package
```

### build and test

```bash
atlas-mvn package verify
```

override `confluence.version` property to test with different versions of Confluence

```bash
atlas-mvn package verify -Dconfluence.version=6.13.13
atlas-mvn package verify -Dconfluence.version=6.6.17
atlas-mvn package verify -Dconfluence.version=7.6.1
```
