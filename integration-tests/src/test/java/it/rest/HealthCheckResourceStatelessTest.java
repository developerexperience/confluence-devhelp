package it.rest;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessRestTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.util.Map;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;

@RunWith(ConfluenceStatelessRestTestRunner.class)
public class HealthCheckResourceStatelessTest {

    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();
    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static PageFixture page = pageFixture()
            .space(space)
            .author(user)
            .title("HealthCheck Resource Page")
            .build();

    @Test
    public void testPing() {
        Map<String, String> response = restClient.createSession(user.get())
                .resource("/rest/devhelp/latest/health/ping")
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(Map.class);

        assertThat(response, hasKey("components"));
    }

    @Test
    public void testContent() {
        Content content = restClient.createSession(user.get())
                .resource("/rest/devhelp/latest/health/content/" + page.get().getId().asLong())
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get(Content.class);
        assertThat(content.getTitle(), is(page.get().getTitle()));
    }
}
